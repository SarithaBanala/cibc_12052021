package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class Account_Created_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "message_field")
	public static WebElement message_field;

public void verify_message_field(String data){
		Assert.assertEquals(message_field,message_field);
}

public void enter_message_field(String data){
		message_field.sendKeys(data);
}

public static void verify_Text(String data){
	Assert.assertTrue(driver.getPageSource().contains(data));
}
}