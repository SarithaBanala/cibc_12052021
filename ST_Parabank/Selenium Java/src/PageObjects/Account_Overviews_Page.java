package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class Account_Overviews_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "_")
	public static WebElement _;

public void verify__(String data){
		Assert.assertEquals(_,_);
}

public void verify___Status(String data){
		//Verifies the Status of the _
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(_.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(_.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!_.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!_.isEnabled());
				break;
			default:
				break;
			}
		}
	}
@FindBy(how= How.ID, using = "Account1_field")
	public static WebElement Account1_field;

public void verify_Account1_field(String data){
		Assert.assertEquals(Account1_field,Account1_field);
}

public void enter_Account1_field(String data){
		Account1_field.sendKeys(data);
}

@FindBy(how= How.ID, using = "Balance_of_Account1_field")
	public static WebElement Balance_of_Account1_field;

public void verify_Balance_of_Account1_field(String data){
		Assert.assertEquals(Balance_of_Account1_field,Balance_of_Account1_field);
}

public void enter_Balance_of_Account1_field(String data){
		Balance_of_Account1_field.sendKeys(data);
}

@FindBy(how= How.ID, using = "Available_Amount_of_Account_1_field")
	public static WebElement Available_Amount_of_Account_1_field;

public void verify_Available_Amount_of_Account_1_field(String data){
		Assert.assertEquals(Available_Amount_of_Account_1_field,Available_Amount_of_Account_1_field);
}

public void enter_Available_Amount_of_Account_1_field(String data){
		Available_Amount_of_Account_1_field.sendKeys(data);
}

@FindBy(how= How.ID, using = "Account2_field")
	public static WebElement Account2_field;

public void verify_Account2_field(String data){
		Assert.assertEquals(Account2_field,Account2_field);
}

public void enter_Account2_field(String data){
		Account2_field.sendKeys(data);
}

@FindBy(how= How.ID, using = "Balance_Account2_field")
	public static WebElement Balance_Account2_field;

public void verify_Balance_Account2_field(String data){
		Assert.assertEquals(Balance_Account2_field,Balance_Account2_field);
}

public void enter_Balance_Account2_field(String data){
		Balance_Account2_field.sendKeys(data);
}

@FindBy(how= How.ID, using = "Avilable_Amount_of_Account2_field")
	public static WebElement Avilable_Amount_of_Account2_field;

public void verify_Avilable_Amount_of_Account2_field(String data){
		Assert.assertEquals(Avilable_Amount_of_Account2_field,Avilable_Amount_of_Account2_field);
}

public void enter_Avilable_Amount_of_Account2_field(String data){
		Avilable_Amount_of_Account2_field.sendKeys(data);
}

@FindBy(how= How.ID, using = "Total_Amount_field")
	public static WebElement Total_Amount_field;

public void verify_Total_Amount_field(String data){
		Assert.assertEquals(Total_Amount_field,Total_Amount_field);
}

public void enter_Total_Amount_field(String data){
		Total_Amount_field.sendKeys(data);
}

public static void verify_Text(String data){
	Assert.assertTrue(driver.getPageSource().contains(data));
}
}