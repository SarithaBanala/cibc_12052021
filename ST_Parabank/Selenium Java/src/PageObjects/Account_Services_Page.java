package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class Account_Services_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "Open_New_Account_hyperlink")
	public static WebElement Open_New_Account_hyperlink;

public void verify_Open_New_Account_hyperlink_Status(String data){
		//Verifies the Status of the Open_New_Account_hyperlink
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Open_New_Account_hyperlink.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Open_New_Account_hyperlink.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Open_New_Account_hyperlink.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Open_New_Account_hyperlink.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_Open_New_Account_hyperlink(){
		Open_New_Account_hyperlink.click();
}

@FindBy(how= How.ID, using = "Account_Overview_hyperlink")
	public static WebElement Account_Overview_hyperlink;

public void verify_Account_Overview_hyperlink_Status(String data){
		//Verifies the Status of the Account_Overview_hyperlink
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Account_Overview_hyperlink.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Account_Overview_hyperlink.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Account_Overview_hyperlink.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Account_Overview_hyperlink.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_Account_Overview_hyperlink(){
		Account_Overview_hyperlink.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_hyperlink")
	public static WebElement Transfer_Funds_hyperlink;

public void verify_Transfer_Funds_hyperlink_Status(String data){
		//Verifies the Status of the Transfer_Funds_hyperlink
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Transfer_Funds_hyperlink.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Transfer_Funds_hyperlink.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Transfer_Funds_hyperlink.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Transfer_Funds_hyperlink.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_Transfer_Funds_hyperlink(){
		Transfer_Funds_hyperlink.click();
}

@FindBy(how= How.ID, using = "Bill_Pay_hyperlink")
	public static WebElement Bill_Pay_hyperlink;

public void verify_Bill_Pay_hyperlink_Status(String data){
		//Verifies the Status of the Bill_Pay_hyperlink
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Bill_Pay_hyperlink.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Bill_Pay_hyperlink.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Bill_Pay_hyperlink.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Bill_Pay_hyperlink.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_Bill_Pay_hyperlink(){
		Bill_Pay_hyperlink.click();
}

@FindBy(how= How.ID, using = "Find_Transactions_hyperlink")
	public static WebElement Find_Transactions_hyperlink;

public void verify_Find_Transactions_hyperlink_Status(String data){
		//Verifies the Status of the Find_Transactions_hyperlink
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Find_Transactions_hyperlink.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Find_Transactions_hyperlink.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Find_Transactions_hyperlink.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Find_Transactions_hyperlink.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_Find_Transactions_hyperlink(){
		Find_Transactions_hyperlink.click();
}

@FindBy(how= How.ID, using = "Update_Contact_Info_hyperlink")
	public static WebElement Update_Contact_Info_hyperlink;

public void verify_Update_Contact_Info_hyperlink_Status(String data){
		//Verifies the Status of the Update_Contact_Info_hyperlink
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Update_Contact_Info_hyperlink.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Update_Contact_Info_hyperlink.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Update_Contact_Info_hyperlink.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Update_Contact_Info_hyperlink.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_Update_Contact_Info_hyperlink(){
		Update_Contact_Info_hyperlink.click();
}

@FindBy(how= How.ID, using = "Request_Loan_hyperlink")
	public static WebElement Request_Loan_hyperlink;

public void verify_Request_Loan_hyperlink_Status(String data){
		//Verifies the Status of the Request_Loan_hyperlink
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Request_Loan_hyperlink.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Request_Loan_hyperlink.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Request_Loan_hyperlink.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Request_Loan_hyperlink.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_Request_Loan_hyperlink(){
		Request_Loan_hyperlink.click();
}

@FindBy(how= How.ID, using = "Log_Out_hyperlink")
	public static WebElement Log_Out_hyperlink;

public void verify_Log_Out_hyperlink_Status(String data){
		//Verifies the Status of the Log_Out_hyperlink
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Log_Out_hyperlink.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Log_Out_hyperlink.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Log_Out_hyperlink.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Log_Out_hyperlink.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_Log_Out_hyperlink(){
		Log_Out_hyperlink.click();
}

public static void verify_Text(String data){
	Assert.assertTrue(driver.getPageSource().contains(data));
}
}