package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class Accounts_Overview_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "Account_field")
	public static WebElement Account_field;

public void verify_Account_field(String data){
		Assert.assertEquals(Account_field,Account_field);
}

public void enter_Account_field(String data){
		Account_field.sendKeys(data);
}

@FindBy(how= How.ID, using = "Balance_field")
	public static WebElement Balance_field;

public void verify_Balance_field(String data){
		Assert.assertEquals(Balance_field,Balance_field);
}

public void enter_Balance_field(String data){
		Balance_field.sendKeys(data);
}

@FindBy(how= How.ID, using = "Available_Amount_field")
	public static WebElement Available_Amount_field;

public void verify_Available_Amount_field(String data){
		Assert.assertEquals(Available_Amount_field,Available_Amount_field);
}

public void enter_Available_Amount_field(String data){
		Available_Amount_field.sendKeys(data);
}

public static void verify_Text(String data){
	Assert.assertTrue(driver.getPageSource().contains(data));
}
}