package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class Administration_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "Loan_Provider_dropdown")
	public static WebElement Loan_Provider_dropdown;

public void verify_Loan_Provider_dropdown(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Loan_Provider_dropdown.getAttribute("value"),data);
	}

}

public void verify_Loan_Provider_dropdown_Status(String data){
		//Verifies the Status of the Loan_Provider_dropdown
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Loan_Provider_dropdown.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Loan_Provider_dropdown.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Loan_Provider_dropdown.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Loan_Provider_dropdown.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void select_Loan_Provider_dropdown(String data){
		Select dropdown= new Select(Loan_Provider_dropdown);
		 dropdown.selectByVisibleText(data);
}

@FindBy(how= How.ID, using = "Loan_Processor_dropdown")
	public static WebElement Loan_Processor_dropdown;

public void verify_Loan_Processor_dropdown(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Loan_Processor_dropdown.getAttribute("value"),data);
	}

}

public void verify_Loan_Processor_dropdown_Status(String data){
		//Verifies the Status of the Loan_Processor_dropdown
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Loan_Processor_dropdown.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Loan_Processor_dropdown.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Loan_Processor_dropdown.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Loan_Processor_dropdown.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void select_Loan_Processor_dropdown(String data){
		Select dropdown= new Select(Loan_Processor_dropdown);
		 dropdown.selectByVisibleText(data);
}

@FindBy(how= How.ID, using = "Threshold_textbox")
	public static WebElement Threshold_textbox;

public void verify_Threshold_textbox(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Threshold_textbox.getAttribute("value"),data);
	}

}

public void verify_Threshold_textbox_Status(String data){
		//Verifies the Status of the Threshold_textbox
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Threshold_textbox.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Threshold_textbox.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Threshold_textbox.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Threshold_textbox.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_Threshold_textbox(String data){
		Threshold_textbox.clear();
		Threshold_textbox.sendKeys(data);
}

@FindBy(how= How.ID, using = "Initial_Balance_textbox")
	public static WebElement Initial_Balance_textbox;

public void verify_Initial_Balance_textbox(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Initial_Balance_textbox.getAttribute("value"),data);
	}

}

public void verify_Initial_Balance_textbox_Status(String data){
		//Verifies the Status of the Initial_Balance_textbox
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Initial_Balance_textbox.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Initial_Balance_textbox.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Initial_Balance_textbox.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Initial_Balance_textbox.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_Initial_Balance_textbox(String data){
		Initial_Balance_textbox.clear();
		Initial_Balance_textbox.sendKeys(data);
}

@FindBy(how= How.ID, using = "Min_Balance_textbox")
	public static WebElement Min_Balance_textbox;

public void verify_Min_Balance_textbox(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Min_Balance_textbox.getAttribute("value"),data);
	}

}

public void verify_Min_Balance_textbox_Status(String data){
		//Verifies the Status of the Min_Balance_textbox
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Min_Balance_textbox.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Min_Balance_textbox.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Min_Balance_textbox.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Min_Balance_textbox.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_Min_Balance_textbox(String data){
		Min_Balance_textbox.clear();
		Min_Balance_textbox.sendKeys(data);
}

@FindBy(how= How.ID, using = "SUBMIT_button")
	public static WebElement SUBMIT_button;

public void verify_SUBMIT_button_Status(String data){
		//Verifies the Status of the SUBMIT_button
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(SUBMIT_button.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(SUBMIT_button.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!SUBMIT_button.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!SUBMIT_button.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_SUBMIT_button(){
		SUBMIT_button.click();
}

@FindBy(how= How.ID, using = "Available_Funds_button")
	public static WebElement Available_Funds_button;

public void verify_Available_Funds_button_Status(String data){
		//Verifies the Status of the Available_Funds_button
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Available_Funds_button.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Available_Funds_button.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Available_Funds_button.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Available_Funds_button.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_Available_Funds_button(){
		Available_Funds_button.click();
}

public static void verify_Text(String data){
	Assert.assertTrue(driver.getPageSource().contains(data));
}
}