package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class Apply_for_a_Loan_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "Loan_Amount_textbox")
	public static WebElement Loan_Amount_textbox;

public void verify_Loan_Amount_textbox(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Loan_Amount_textbox.getAttribute("value"),data);
	}

}

public void verify_Loan_Amount_textbox_Status(String data){
		//Verifies the Status of the Loan_Amount_textbox
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Loan_Amount_textbox.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Loan_Amount_textbox.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Loan_Amount_textbox.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Loan_Amount_textbox.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_Loan_Amount_textbox(String data){
		Loan_Amount_textbox.clear();
		Loan_Amount_textbox.sendKeys(data);
}

@FindBy(how= How.ID, using = "Down_Payment_textbox")
	public static WebElement Down_Payment_textbox;

public void verify_Down_Payment_textbox(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Down_Payment_textbox.getAttribute("value"),data);
	}

}

public void verify_Down_Payment_textbox_Status(String data){
		//Verifies the Status of the Down_Payment_textbox
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Down_Payment_textbox.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Down_Payment_textbox.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Down_Payment_textbox.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Down_Payment_textbox.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_Down_Payment_textbox(String data){
		Down_Payment_textbox.clear();
		Down_Payment_textbox.sendKeys(data);
}

@FindBy(how= How.ID, using = "From_Account_dropdown")
	public static WebElement From_Account_dropdown;

public void verify_From_Account_dropdown(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(From_Account_dropdown.getAttribute("value"),data);
	}

}

public void verify_From_Account_dropdown_Status(String data){
		//Verifies the Status of the From_Account_dropdown
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(From_Account_dropdown.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(From_Account_dropdown.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!From_Account_dropdown.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!From_Account_dropdown.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void select_From_Account_dropdown(String data){
		Select dropdown= new Select(From_Account_dropdown);
		 dropdown.selectByVisibleText(data);
}

@FindBy(how= How.ID, using = "Apply_Now_button")
	public static WebElement Apply_Now_button;

public void verify_Apply_Now_button_Status(String data){
		//Verifies the Status of the Apply_Now_button
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Apply_Now_button.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Apply_Now_button.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Apply_Now_button.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Apply_Now_button.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_Apply_Now_button(){
		Apply_Now_button.click();
}

@FindBy(how= How.ID, using = "Account_Overview_hyperlink")
	public static WebElement Account_Overview_hyperlink;

public void verify_Account_Overview_hyperlink_Status(String data){
		//Verifies the Status of the Account_Overview_hyperlink
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Account_Overview_hyperlink.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Account_Overview_hyperlink.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Account_Overview_hyperlink.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Account_Overview_hyperlink.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_Account_Overview_hyperlink(){
		Account_Overview_hyperlink.click();
}

@FindBy(how= How.ID, using = "Existing_Accounts_hyperlink")
	public static WebElement Existing_Accounts_hyperlink;

public void verify_Existing_Accounts_hyperlink_Status(String data){
		//Verifies the Status of the Existing_Accounts_hyperlink
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Existing_Accounts_hyperlink.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Existing_Accounts_hyperlink.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Existing_Accounts_hyperlink.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Existing_Accounts_hyperlink.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_Existing_Accounts_hyperlink(){
		Existing_Accounts_hyperlink.click();
}

public static void verify_Text(String data){
	Assert.assertTrue(driver.getPageSource().contains(data));
}
}