package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class Bill_Payment_Service_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "Payee_Name_textbox")
	public static WebElement Payee_Name_textbox;

public void verify_Payee_Name_textbox(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Payee_Name_textbox.getAttribute("value"),data);
	}

}

public void verify_Payee_Name_textbox_Status(String data){
		//Verifies the Status of the Payee_Name_textbox
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Payee_Name_textbox.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Payee_Name_textbox.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Payee_Name_textbox.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Payee_Name_textbox.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_Payee_Name_textbox(String data){
		Payee_Name_textbox.clear();
		Payee_Name_textbox.sendKeys(data);
}

@FindBy(how= How.ID, using = "Address_textbox")
	public static WebElement Address_textbox;

public void verify_Address_textbox(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Address_textbox.getAttribute("value"),data);
	}

}

public void verify_Address_textbox_Status(String data){
		//Verifies the Status of the Address_textbox
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Address_textbox.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Address_textbox.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Address_textbox.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Address_textbox.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_Address_textbox(String data){
		Address_textbox.clear();
		Address_textbox.sendKeys(data);
}

@FindBy(how= How.ID, using = "City_textbox")
	public static WebElement City_textbox;

public void verify_City_textbox(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(City_textbox.getAttribute("value"),data);
	}

}

public void verify_City_textbox_Status(String data){
		//Verifies the Status of the City_textbox
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(City_textbox.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(City_textbox.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!City_textbox.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!City_textbox.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_City_textbox(String data){
		City_textbox.clear();
		City_textbox.sendKeys(data);
}

@FindBy(how= How.ID, using = "State_textbox")
	public static WebElement State_textbox;

public void verify_State_textbox(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(State_textbox.getAttribute("value"),data);
	}

}

public void verify_State_textbox_Status(String data){
		//Verifies the Status of the State_textbox
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(State_textbox.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(State_textbox.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!State_textbox.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!State_textbox.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_State_textbox(String data){
		State_textbox.clear();
		State_textbox.sendKeys(data);
}

@FindBy(how= How.ID, using = "Zip_Code_textbox")
	public static WebElement Zip_Code_textbox;

public void verify_Zip_Code_textbox(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Zip_Code_textbox.getAttribute("value"),data);
	}

}

public void verify_Zip_Code_textbox_Status(String data){
		//Verifies the Status of the Zip_Code_textbox
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Zip_Code_textbox.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Zip_Code_textbox.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Zip_Code_textbox.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Zip_Code_textbox.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_Zip_Code_textbox(String data){
		Zip_Code_textbox.clear();
		Zip_Code_textbox.sendKeys(data);
}

@FindBy(how= How.ID, using = "Phone_textbox")
	public static WebElement Phone_textbox;

public void verify_Phone_textbox(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Phone_textbox.getAttribute("value"),data);
	}

}

public void verify_Phone_textbox_Status(String data){
		//Verifies the Status of the Phone_textbox
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Phone_textbox.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Phone_textbox.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Phone_textbox.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Phone_textbox.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_Phone_textbox(String data){
		Phone_textbox.clear();
		Phone_textbox.sendKeys(data);
}

@FindBy(how= How.ID, using = "Account_textbox")
	public static WebElement Account_textbox;

public void verify_Account_textbox(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Account_textbox.getAttribute("value"),data);
	}

}

public void verify_Account_textbox_Status(String data){
		//Verifies the Status of the Account_textbox
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Account_textbox.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Account_textbox.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Account_textbox.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Account_textbox.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_Account_textbox(String data){
		Account_textbox.clear();
		Account_textbox.sendKeys(data);
}

@FindBy(how= How.ID, using = "Verify_Account_textbox")
	public static WebElement Verify_Account_textbox;

public void verify_Verify_Account_textbox(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Verify_Account_textbox.getAttribute("value"),data);
	}

}

public void verify_Verify_Account_textbox_Status(String data){
		//Verifies the Status of the Verify_Account_textbox
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Verify_Account_textbox.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Verify_Account_textbox.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Verify_Account_textbox.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Verify_Account_textbox.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_Verify_Account_textbox(String data){
		Verify_Account_textbox.clear();
		Verify_Account_textbox.sendKeys(data);
}

@FindBy(how= How.ID, using = "Amount_textbox")
	public static WebElement Amount_textbox;

public void verify_Amount_textbox(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Amount_textbox.getAttribute("value"),data);
	}

}

public void verify_Amount_textbox_Status(String data){
		//Verifies the Status of the Amount_textbox
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Amount_textbox.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Amount_textbox.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Amount_textbox.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Amount_textbox.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_Amount_textbox(String data){
		Amount_textbox.clear();
		Amount_textbox.sendKeys(data);
}

@FindBy(how= How.ID, using = "From_Account_dropdown")
	public static WebElement From_Account_dropdown;

public void verify_From_Account_dropdown(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(From_Account_dropdown.getAttribute("value"),data);
	}

}

public void verify_From_Account_dropdown_Status(String data){
		//Verifies the Status of the From_Account_dropdown
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(From_Account_dropdown.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(From_Account_dropdown.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!From_Account_dropdown.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!From_Account_dropdown.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void select_From_Account_dropdown(String data){
		Select dropdown= new Select(From_Account_dropdown);
		 dropdown.selectByVisibleText(data);
}

@FindBy(how= How.ID, using = "SEND_PAYMENT_button")
	public static WebElement SEND_PAYMENT_button;

public void verify_SEND_PAYMENT_button_Status(String data){
		//Verifies the Status of the SEND_PAYMENT_button
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(SEND_PAYMENT_button.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(SEND_PAYMENT_button.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!SEND_PAYMENT_button.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!SEND_PAYMENT_button.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_SEND_PAYMENT_button(){
		SEND_PAYMENT_button.click();
}

public static void verify_Text(String data){
	Assert.assertTrue(driver.getPageSource().contains(data));
}
}