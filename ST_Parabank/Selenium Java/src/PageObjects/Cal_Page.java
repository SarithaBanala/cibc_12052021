package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class Cal_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "Num1_textbox")
	public static WebElement Num1_textbox;

public void verify_Num1_textbox(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Num1_textbox.getAttribute("value"),data);
	}

}

public void verify_Num1_textbox_Status(String data){
		//Verifies the Status of the Num1_textbox
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Num1_textbox.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Num1_textbox.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Num1_textbox.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Num1_textbox.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_Num1_textbox(String data){
		Num1_textbox.clear();
		Num1_textbox.sendKeys(data);
}

@FindBy(how= How.ID, using = "Num2_textbox")
	public static WebElement Num2_textbox;

public void verify_Num2_textbox(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Num2_textbox.getAttribute("value"),data);
	}

}

public void verify_Num2_textbox_Status(String data){
		//Verifies the Status of the Num2_textbox
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Num2_textbox.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Num2_textbox.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Num2_textbox.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Num2_textbox.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_Num2_textbox(String data){
		Num2_textbox.clear();
		Num2_textbox.sendKeys(data);
}

@FindBy(how= How.ID, using = "Res_textbox")
	public static WebElement Res_textbox;

public void verify_Res_textbox(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Res_textbox.getAttribute("value"),data);
	}

}

public void verify_Res_textbox_Status(String data){
		//Verifies the Status of the Res_textbox
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Res_textbox.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Res_textbox.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Res_textbox.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Res_textbox.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_Res_textbox(String data){
		Res_textbox.clear();
		Res_textbox.sendKeys(data);
}

@FindBy(how= How.ID, using = "Operator_dropdown")
	public static WebElement Operator_dropdown;

public void verify_Operator_dropdown(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Operator_dropdown.getAttribute("value"),data);
	}

}

public void verify_Operator_dropdown_Status(String data){
		//Verifies the Status of the Operator_dropdown
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Operator_dropdown.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Operator_dropdown.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Operator_dropdown.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Operator_dropdown.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void select_Operator_dropdown(String data){
		Select dropdown= new Select(Operator_dropdown);
		 dropdown.selectByVisibleText(data);
}

public static void verify_Text(String data){
	Assert.assertTrue(driver.getPageSource().contains(data));
}
}