package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class Find_Transactions_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "Select_an_account_dropdown")
	public static WebElement Select_an_account_dropdown;

public void verify_Select_an_account_dropdown(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Select_an_account_dropdown.getAttribute("value"),data);
	}

}

public void verify_Select_an_account_dropdown_Status(String data){
		//Verifies the Status of the Select_an_account_dropdown
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Select_an_account_dropdown.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Select_an_account_dropdown.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Select_an_account_dropdown.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Select_an_account_dropdown.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void select_Select_an_account_dropdown(String data){
		Select dropdown= new Select(Select_an_account_dropdown);
		 dropdown.selectByVisibleText(data);
}

@FindBy(how= How.ID, using = "Find_By_Transaction_ID_textbox")
	public static WebElement Find_By_Transaction_ID_textbox;

public void verify_Find_By_Transaction_ID_textbox(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Find_By_Transaction_ID_textbox.getAttribute("value"),data);
	}

}

public void verify_Find_By_Transaction_ID_textbox_Status(String data){
		//Verifies the Status of the Find_By_Transaction_ID_textbox
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Find_By_Transaction_ID_textbox.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Find_By_Transaction_ID_textbox.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Find_By_Transaction_ID_textbox.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Find_By_Transaction_ID_textbox.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_Find_By_Transaction_ID_textbox(String data){
		Find_By_Transaction_ID_textbox.clear();
		Find_By_Transaction_ID_textbox.sendKeys(data);
}

@FindBy(how= How.ID, using = "Find_Transactions_button")
	public static WebElement Find_Transactions_button;

public void verify_Find_Transactions_button_Status(String data){
		//Verifies the Status of the Find_Transactions_button
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Find_Transactions_button.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Find_Transactions_button.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Find_Transactions_button.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Find_Transactions_button.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_Find_Transactions_button(){
		Find_Transactions_button.click();
}

@FindBy(how= How.ID, using = "Between_textbox")
	public static WebElement Between_textbox;

public void verify_Between_textbox(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Between_textbox.getAttribute("value"),data);
	}

}

public void verify_Between_textbox_Status(String data){
		//Verifies the Status of the Between_textbox
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Between_textbox.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Between_textbox.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Between_textbox.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Between_textbox.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_Between_textbox(String data){
		Between_textbox.clear();
		Between_textbox.sendKeys(data);
}

@FindBy(how= How.ID, using = "And_textbox")
	public static WebElement And_textbox;

public void verify_And_textbox(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(And_textbox.getAttribute("value"),data);
	}

}

public void verify_And_textbox_Status(String data){
		//Verifies the Status of the And_textbox
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(And_textbox.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(And_textbox.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!And_textbox.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!And_textbox.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_And_textbox(String data){
		And_textbox.clear();
		And_textbox.sendKeys(data);
}

@FindBy(how= How.ID, using = "Find_By_Amount_textbox")
	public static WebElement Find_By_Amount_textbox;

public void verify_Find_By_Amount_textbox(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Find_By_Amount_textbox.getAttribute("value"),data);
	}

}

public void verify_Find_By_Amount_textbox_Status(String data){
		//Verifies the Status of the Find_By_Amount_textbox
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Find_By_Amount_textbox.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Find_By_Amount_textbox.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Find_By_Amount_textbox.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Find_By_Amount_textbox.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_Find_By_Amount_textbox(String data){
		Find_By_Amount_textbox.clear();
		Find_By_Amount_textbox.sendKeys(data);
}

@FindBy(how= How.ID, using = "Find_By_Date_textbox")
	public static WebElement Find_By_Date_textbox;

public void verify_Find_By_Date_textbox(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Find_By_Date_textbox.getAttribute("value"),data);
	}

}

public void verify_Find_By_Date_textbox_Status(String data){
		//Verifies the Status of the Find_By_Date_textbox
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Find_By_Date_textbox.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Find_By_Date_textbox.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Find_By_Date_textbox.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Find_By_Date_textbox.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_Find_By_Date_textbox(String data){
		Find_By_Date_textbox.clear();
		Find_By_Date_textbox.sendKeys(data);
}

public static void verify_Text(String data){
	Assert.assertTrue(driver.getPageSource().contains(data));
}
}