package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class Invalid_Register_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "Invalid_Register_field")
	public static WebElement Invalid_Register_field;

public void verify_Invalid_Register_field(String data){
		Assert.assertEquals(Invalid_Register_field,Invalid_Register_field);
}

public void enter_Invalid_Register_field(String data){
		Invalid_Register_field.sendKeys(data);
}

public static void verify_Text(String data){
	Assert.assertTrue(driver.getPageSource().contains(data));
}
}