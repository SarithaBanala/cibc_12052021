package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class Loan_Request_Processed_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "Loan_Provider_field")
	public static WebElement Loan_Provider_field;

public void verify_Loan_Provider_field(String data){
		Assert.assertEquals(Loan_Provider_field,Loan_Provider_field);
}

public void enter_Loan_Provider_field(String data){
		Loan_Provider_field.sendKeys(data);
}

@FindBy(how= How.ID, using = "Date_field")
	public static WebElement Date_field;

public void verify_Date_field(String data){
		Assert.assertEquals(Date_field,Date_field);
}

public void enter_Date_field(String data){
		Date_field.sendKeys(data);
}

@FindBy(how= How.ID, using = "Status_field")
	public static WebElement Status_field;

public void verify_Status_field(String data){
		Assert.assertEquals(Status_field,Status_field);
}

public void enter_Status_field(String data){
		Status_field.sendKeys(data);
}

@FindBy(how= How.ID, using = "message_field")
	public static WebElement message_field;

public void verify_message_field(String data){
		Assert.assertEquals(message_field,message_field);
}

public void enter_message_field(String data){
		message_field.sendKeys(data);
}

@FindBy(how= How.ID, using = "Account_Number_field")
	public static WebElement Account_Number_field;

public void verify_Account_Number_field(String data){
		Assert.assertEquals(Account_Number_field,Account_Number_field);
}

public void enter_Account_Number_field(String data){
		Account_Number_field.sendKeys(data);
}

public static void verify_Text(String data){
	Assert.assertTrue(driver.getPageSource().contains(data));
}
}