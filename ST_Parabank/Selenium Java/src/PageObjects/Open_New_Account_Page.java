package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class Open_New_Account_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "Account_Type_dropdown")
	public static WebElement Account_Type_dropdown;

public void verify_Account_Type_dropdown(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Account_Type_dropdown.getAttribute("value"),data);
	}

}

public void verify_Account_Type_dropdown_Status(String data){
		//Verifies the Status of the Account_Type_dropdown
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Account_Type_dropdown.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Account_Type_dropdown.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Account_Type_dropdown.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Account_Type_dropdown.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void select_Account_Type_dropdown(String data){
		Select dropdown= new Select(Account_Type_dropdown);
		 dropdown.selectByVisibleText(data);
}

@FindBy(how= How.ID, using = "Existing_Account_dropdown")
	public static WebElement Existing_Account_dropdown;

public void verify_Existing_Account_dropdown(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Existing_Account_dropdown.getAttribute("value"),data);
	}

}

public void verify_Existing_Account_dropdown_Status(String data){
		//Verifies the Status of the Existing_Account_dropdown
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Existing_Account_dropdown.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Existing_Account_dropdown.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Existing_Account_dropdown.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Existing_Account_dropdown.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void select_Existing_Account_dropdown(String data){
		Select dropdown= new Select(Existing_Account_dropdown);
		 dropdown.selectByVisibleText(data);
}

@FindBy(how= How.ID, using = "Open_New_Account_button")
	public static WebElement Open_New_Account_button;

public void verify_Open_New_Account_button_Status(String data){
		//Verifies the Status of the Open_New_Account_button
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Open_New_Account_button.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Open_New_Account_button.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Open_New_Account_button.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Open_New_Account_button.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_Open_New_Account_button(){
		Open_New_Account_button.click();
}

@FindBy(how= How.ID, using = "Account_Overview_hyperlink")
	public static WebElement Account_Overview_hyperlink;

public void verify_Account_Overview_hyperlink_Status(String data){
		//Verifies the Status of the Account_Overview_hyperlink
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Account_Overview_hyperlink.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Account_Overview_hyperlink.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Account_Overview_hyperlink.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Account_Overview_hyperlink.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_Account_Overview_hyperlink(){
		Account_Overview_hyperlink.click();
}

@FindBy(how= How.ID, using = "Request_Loan_hyperlink")
	public static WebElement Request_Loan_hyperlink;

public void verify_Request_Loan_hyperlink_Status(String data){
		//Verifies the Status of the Request_Loan_hyperlink
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Request_Loan_hyperlink.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Request_Loan_hyperlink.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Request_Loan_hyperlink.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Request_Loan_hyperlink.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_Request_Loan_hyperlink(){
		Request_Loan_hyperlink.click();
}

@FindBy(how= How.ID, using = "Existing_Account_hyperlink")
	public static WebElement Existing_Account_hyperlink;

public void verify_Existing_Account_hyperlink_Status(String data){
		//Verifies the Status of the Existing_Account_hyperlink
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Existing_Account_hyperlink.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Existing_Account_hyperlink.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Existing_Account_hyperlink.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Existing_Account_hyperlink.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_Existing_Account_hyperlink(){
		Existing_Account_hyperlink.click();
}

public static void verify_Text(String data){
	Assert.assertTrue(driver.getPageSource().contains(data));
}
}