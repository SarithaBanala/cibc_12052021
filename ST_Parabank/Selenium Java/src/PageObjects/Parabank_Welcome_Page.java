package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class Parabank_Welcome_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "Password_textbox")
	public static WebElement Password_textbox;

public void verify_Password_textbox(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Password_textbox.getAttribute("value"),data);
	}

}

public void verify_Password_textbox_Status(String data){
		//Verifies the Status of the Password_textbox
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Password_textbox.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Password_textbox.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Password_textbox.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Password_textbox.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_Password_textbox(String data){
		Password_textbox.clear();
		Password_textbox.sendKeys(data);
}

@FindBy(how= How.ID, using = "Username_textbox")
	public static WebElement Username_textbox;

public void verify_Username_textbox(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Username_textbox.getAttribute("value"),data);
	}

}

public void verify_Username_textbox_Status(String data){
		//Verifies the Status of the Username_textbox
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Username_textbox.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Username_textbox.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Username_textbox.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Username_textbox.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_Username_textbox(String data){
		Username_textbox.clear();
		Username_textbox.sendKeys(data);
}

@FindBy(how= How.ID, using = "LOG_IN_button")
	public static WebElement LOG_IN_button;

public void verify_LOG_IN_button_Status(String data){
		//Verifies the Status of the LOG_IN_button
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(LOG_IN_button.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(LOG_IN_button.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!LOG_IN_button.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!LOG_IN_button.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_LOG_IN_button(){
		LOG_IN_button.click();
}

@FindBy(how= How.ID, using = "Register_hyperlink")
	public static WebElement Register_hyperlink;

public void verify_Register_hyperlink_Status(String data){
		//Verifies the Status of the Register_hyperlink
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Register_hyperlink.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Register_hyperlink.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Register_hyperlink.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Register_hyperlink.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_Register_hyperlink(){
		Register_hyperlink.click();
}

@FindBy(how= How.ID, using = "Forgot_login_info__hyperlink")
	public static WebElement Forgot_login_info__hyperlink;

public void verify_Forgot_login_info__hyperlink_Status(String data){
		//Verifies the Status of the Forgot_login_info__hyperlink
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Forgot_login_info__hyperlink.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Forgot_login_info__hyperlink.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Forgot_login_info__hyperlink.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Forgot_login_info__hyperlink.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_Forgot_login_info__hyperlink(){
		Forgot_login_info__hyperlink.click();
}

@FindBy(how= How.ID, using = "Admin_Page_hyperlink")
	public static WebElement Admin_Page_hyperlink;

public void verify_Admin_Page_hyperlink_Status(String data){
		//Verifies the Status of the Admin_Page_hyperlink
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Admin_Page_hyperlink.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Admin_Page_hyperlink.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Admin_Page_hyperlink.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Admin_Page_hyperlink.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_Admin_Page_hyperlink(){
		Admin_Page_hyperlink.click();
}

public static void verify_Text(String data){
	Assert.assertTrue(driver.getPageSource().contains(data));
}
}