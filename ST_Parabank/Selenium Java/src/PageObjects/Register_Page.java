package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class Register_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "First_Name_textbox")
	public static WebElement First_Name_textbox;

public void verify_First_Name_textbox(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(First_Name_textbox.getAttribute("value"),data);
	}

}

public void verify_First_Name_textbox_Status(String data){
		//Verifies the Status of the First_Name_textbox
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(First_Name_textbox.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(First_Name_textbox.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!First_Name_textbox.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!First_Name_textbox.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_First_Name_textbox(String data){
		First_Name_textbox.clear();
		First_Name_textbox.sendKeys(data);
}

@FindBy(how= How.ID, using = "Last_Name_textbox")
	public static WebElement Last_Name_textbox;

public void verify_Last_Name_textbox(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Last_Name_textbox.getAttribute("value"),data);
	}

}

public void verify_Last_Name_textbox_Status(String data){
		//Verifies the Status of the Last_Name_textbox
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Last_Name_textbox.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Last_Name_textbox.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Last_Name_textbox.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Last_Name_textbox.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_Last_Name_textbox(String data){
		Last_Name_textbox.clear();
		Last_Name_textbox.sendKeys(data);
}

@FindBy(how= How.ID, using = "Address_textbox")
	public static WebElement Address_textbox;

public void verify_Address_textbox(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Address_textbox.getAttribute("value"),data);
	}

}

public void verify_Address_textbox_Status(String data){
		//Verifies the Status of the Address_textbox
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Address_textbox.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Address_textbox.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Address_textbox.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Address_textbox.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_Address_textbox(String data){
		Address_textbox.clear();
		Address_textbox.sendKeys(data);
}

@FindBy(how= How.ID, using = "City_textbox")
	public static WebElement City_textbox;

public void verify_City_textbox(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(City_textbox.getAttribute("value"),data);
	}

}

public void verify_City_textbox_Status(String data){
		//Verifies the Status of the City_textbox
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(City_textbox.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(City_textbox.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!City_textbox.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!City_textbox.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_City_textbox(String data){
		City_textbox.clear();
		City_textbox.sendKeys(data);
}

@FindBy(how= How.ID, using = "State_textbox")
	public static WebElement State_textbox;

public void verify_State_textbox(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(State_textbox.getAttribute("value"),data);
	}

}

public void verify_State_textbox_Status(String data){
		//Verifies the Status of the State_textbox
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(State_textbox.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(State_textbox.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!State_textbox.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!State_textbox.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_State_textbox(String data){
		State_textbox.clear();
		State_textbox.sendKeys(data);
}

@FindBy(how= How.ID, using = "Zip_Code_textbox")
	public static WebElement Zip_Code_textbox;

public void verify_Zip_Code_textbox(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Zip_Code_textbox.getAttribute("value"),data);
	}

}

public void verify_Zip_Code_textbox_Status(String data){
		//Verifies the Status of the Zip_Code_textbox
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Zip_Code_textbox.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Zip_Code_textbox.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Zip_Code_textbox.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Zip_Code_textbox.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_Zip_Code_textbox(String data){
		Zip_Code_textbox.clear();
		Zip_Code_textbox.sendKeys(data);
}

@FindBy(how= How.ID, using = "Phone_textbox")
	public static WebElement Phone_textbox;

public void verify_Phone_textbox(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Phone_textbox.getAttribute("value"),data);
	}

}

public void verify_Phone_textbox_Status(String data){
		//Verifies the Status of the Phone_textbox
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Phone_textbox.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Phone_textbox.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Phone_textbox.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Phone_textbox.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_Phone_textbox(String data){
		Phone_textbox.clear();
		Phone_textbox.sendKeys(data);
}

@FindBy(how= How.ID, using = "SSN_textbox")
	public static WebElement SSN_textbox;

public void verify_SSN_textbox(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(SSN_textbox.getAttribute("value"),data);
	}

}

public void verify_SSN_textbox_Status(String data){
		//Verifies the Status of the SSN_textbox
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(SSN_textbox.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(SSN_textbox.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!SSN_textbox.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!SSN_textbox.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_SSN_textbox(String data){
		SSN_textbox.clear();
		SSN_textbox.sendKeys(data);
}

@FindBy(how= How.ID, using = "Username_textbox")
	public static WebElement Username_textbox;

public void verify_Username_textbox(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Username_textbox.getAttribute("value"),data);
	}

}

public void verify_Username_textbox_Status(String data){
		//Verifies the Status of the Username_textbox
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Username_textbox.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Username_textbox.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Username_textbox.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Username_textbox.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_Username_textbox(String data){
		Username_textbox.clear();
		Username_textbox.sendKeys(data);
}

@FindBy(how= How.ID, using = "Password_textbox")
	public static WebElement Password_textbox;

public void verify_Password_textbox(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Password_textbox.getAttribute("value"),data);
	}

}

public void verify_Password_textbox_Status(String data){
		//Verifies the Status of the Password_textbox
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Password_textbox.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Password_textbox.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Password_textbox.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Password_textbox.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_Password_textbox(String data){
		Password_textbox.clear();
		Password_textbox.sendKeys(data);
}

@FindBy(how= How.ID, using = "Confirm_textbox")
	public static WebElement Confirm_textbox;

public void verify_Confirm_textbox(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Confirm_textbox.getAttribute("value"),data);
	}

}

public void verify_Confirm_textbox_Status(String data){
		//Verifies the Status of the Confirm_textbox
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Confirm_textbox.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Confirm_textbox.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Confirm_textbox.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Confirm_textbox.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_Confirm_textbox(String data){
		Confirm_textbox.clear();
		Confirm_textbox.sendKeys(data);
}

@FindBy(how= How.ID, using = "RIGISTER_button")
	public static WebElement RIGISTER_button;

public void verify_RIGISTER_button_Status(String data){
		//Verifies the Status of the RIGISTER_button
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(RIGISTER_button.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(RIGISTER_button.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!RIGISTER_button.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!RIGISTER_button.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_RIGISTER_button(){
		RIGISTER_button.click();
}

public static void verify_Text(String data){
	Assert.assertTrue(driver.getPageSource().contains(data));
}
}