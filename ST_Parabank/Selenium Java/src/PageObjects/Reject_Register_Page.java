package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class Reject_Register_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "First_Name_field")
	public static WebElement First_Name_field;

public void verify_First_Name_field(String data){
		Assert.assertEquals(First_Name_field,First_Name_field);
}

public void enter_First_Name_field(String data){
		First_Name_field.sendKeys(data);
}

@FindBy(how= How.ID, using = "Last_Name_field")
	public static WebElement Last_Name_field;

public void verify_Last_Name_field(String data){
		Assert.assertEquals(Last_Name_field,Last_Name_field);
}

public void enter_Last_Name_field(String data){
		Last_Name_field.sendKeys(data);
}

@FindBy(how= How.ID, using = "Adress_field")
	public static WebElement Adress_field;

public void verify_Adress_field(String data){
		Assert.assertEquals(Adress_field,Adress_field);
}

public void enter_Adress_field(String data){
		Adress_field.sendKeys(data);
}

@FindBy(how= How.ID, using = "City_field")
	public static WebElement City_field;

public void verify_City_field(String data){
		Assert.assertEquals(City_field,City_field);
}

public void enter_City_field(String data){
		City_field.sendKeys(data);
}

@FindBy(how= How.ID, using = "State_field")
	public static WebElement State_field;

public void verify_State_field(String data){
		Assert.assertEquals(State_field,State_field);
}

public void enter_State_field(String data){
		State_field.sendKeys(data);
}

@FindBy(how= How.ID, using = "Zip_Code_field")
	public static WebElement Zip_Code_field;

public void verify_Zip_Code_field(String data){
		Assert.assertEquals(Zip_Code_field,Zip_Code_field);
}

public void enter_Zip_Code_field(String data){
		Zip_Code_field.sendKeys(data);
}

@FindBy(how= How.ID, using = "Phone_field")
	public static WebElement Phone_field;

public void verify_Phone_field(String data){
		Assert.assertEquals(Phone_field,Phone_field);
}

public void enter_Phone_field(String data){
		Phone_field.sendKeys(data);
}

@FindBy(how= How.ID, using = "SSN_field")
	public static WebElement SSN_field;

public void verify_SSN_field(String data){
		Assert.assertEquals(SSN_field,SSN_field);
}

public void enter_SSN_field(String data){
		SSN_field.sendKeys(data);
}

@FindBy(how= How.ID, using = "UserName_field")
	public static WebElement UserName_field;

public void verify_UserName_field(String data){
		Assert.assertEquals(UserName_field,UserName_field);
}

public void enter_UserName_field(String data){
		UserName_field.sendKeys(data);
}

@FindBy(how= How.ID, using = "Password_field")
	public static WebElement Password_field;

public void verify_Password_field(String data){
		Assert.assertEquals(Password_field,Password_field);
}

public void enter_Password_field(String data){
		Password_field.sendKeys(data);
}

@FindBy(how= How.ID, using = "Confirm_field")
	public static WebElement Confirm_field;

public void verify_Confirm_field(String data){
		Assert.assertEquals(Confirm_field,Confirm_field);
}

public void enter_Confirm_field(String data){
		Confirm_field.sendKeys(data);
}

public static void verify_Text(String data){
	Assert.assertTrue(driver.getPageSource().contains(data));
}
}