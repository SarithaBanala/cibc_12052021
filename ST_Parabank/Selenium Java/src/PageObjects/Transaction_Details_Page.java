package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class Transaction_Details_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "Transaction_ID_field")
	public static WebElement Transaction_ID_field;

public void verify_Transaction_ID_field(String data){
		Assert.assertEquals(Transaction_ID_field,Transaction_ID_field);
}

public void enter_Transaction_ID_field(String data){
		Transaction_ID_field.sendKeys(data);
}

@FindBy(how= How.ID, using = "Date_field")
	public static WebElement Date_field;

public void verify_Date_field(String data){
		Assert.assertEquals(Date_field,Date_field);
}

public void enter_Date_field(String data){
		Date_field.sendKeys(data);
}

@FindBy(how= How.ID, using = "Description_field")
	public static WebElement Description_field;

public void verify_Description_field(String data){
		Assert.assertEquals(Description_field,Description_field);
}

public void enter_Description_field(String data){
		Description_field.sendKeys(data);
}

@FindBy(how= How.ID, using = "Type_field")
	public static WebElement Type_field;

public void verify_Type_field(String data){
		Assert.assertEquals(Type_field,Type_field);
}

public void enter_Type_field(String data){
		Type_field.sendKeys(data);
}

@FindBy(how= How.ID, using = "Amount_field")
	public static WebElement Amount_field;

public void verify_Amount_field(String data){
		Assert.assertEquals(Amount_field,Amount_field);
}

public void enter_Amount_field(String data){
		Amount_field.sendKeys(data);
}

public static void verify_Text(String data){
	Assert.assertTrue(driver.getPageSource().contains(data));
}
}