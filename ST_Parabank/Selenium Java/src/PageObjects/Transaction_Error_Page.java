package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class Transaction_Error_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "Error_field")
	public static WebElement Error_field;

public void verify_Error_field(String data){
		Assert.assertEquals(Error_field,Error_field);
}

public void enter_Error_field(String data){
		Error_field.sendKeys(data);
}

public static void verify_Text(String data){
	Assert.assertTrue(driver.getPageSource().contains(data));
}
}