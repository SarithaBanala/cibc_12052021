package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class Transaction_Result_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "Result_uitable")
	public static WebElement Result_uitable;

public void verify_Result_uitable(String data){
		Assert.assertEquals(Result_uitable,Result_uitable);
}

public static void verify_Text(String data){
	Assert.assertTrue(driver.getPageSource().contains(data));
}
}