package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class Transfer_Funds_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "Amount_field")
	public static WebElement Amount_field;

public void verify_Amount_field(String data){
		Assert.assertEquals(Amount_field,Amount_field);
}

public void enter_Amount_field(String data){
		Amount_field.sendKeys(data);
}

@FindBy(how= How.ID, using = "From_Account_field")
	public static WebElement From_Account_field;

public void verify_From_Account_field(String data){
		Assert.assertEquals(From_Account_field,From_Account_field);
}

public void enter_From_Account_field(String data){
		From_Account_field.sendKeys(data);
}

@FindBy(how= How.ID, using = "To_Account_field")
	public static WebElement To_Account_field;

public void verify_To_Account_field(String data){
		Assert.assertEquals(To_Account_field,To_Account_field);
}

public void enter_To_Account_field(String data){
		To_Account_field.sendKeys(data);
}

@FindBy(how= How.ID, using = "Amount_textbox")
	public static WebElement Amount_textbox;

public void verify_Amount_textbox(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Amount_textbox.getAttribute("value"),data);
	}

}

public void verify_Amount_textbox_Status(String data){
		//Verifies the Status of the Amount_textbox
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Amount_textbox.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Amount_textbox.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Amount_textbox.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Amount_textbox.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_Amount_textbox(String data){
		Amount_textbox.clear();
		Amount_textbox.sendKeys(data);
}

@FindBy(how= How.ID, using = "From_Account_dropdown")
	public static WebElement From_Account_dropdown;

public void verify_From_Account_dropdown(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(From_Account_dropdown.getAttribute("value"),data);
	}

}

public void verify_From_Account_dropdown_Status(String data){
		//Verifies the Status of the From_Account_dropdown
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(From_Account_dropdown.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(From_Account_dropdown.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!From_Account_dropdown.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!From_Account_dropdown.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void select_From_Account_dropdown(String data){
		Select dropdown= new Select(From_Account_dropdown);
		 dropdown.selectByVisibleText(data);
}

@FindBy(how= How.ID, using = "To_Account_dropdown")
	public static WebElement To_Account_dropdown;

public void verify_To_Account_dropdown(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(To_Account_dropdown.getAttribute("value"),data);
	}

}

public void verify_To_Account_dropdown_Status(String data){
		//Verifies the Status of the To_Account_dropdown
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(To_Account_dropdown.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(To_Account_dropdown.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!To_Account_dropdown.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!To_Account_dropdown.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void select_To_Account_dropdown(String data){
		Select dropdown= new Select(To_Account_dropdown);
		 dropdown.selectByVisibleText(data);
}

@FindBy(how= How.ID, using = "TRANSFER_button")
	public static WebElement TRANSFER_button;

public void verify_TRANSFER_button_Status(String data){
		//Verifies the Status of the TRANSFER_button
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(TRANSFER_button.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(TRANSFER_button.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!TRANSFER_button.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!TRANSFER_button.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_TRANSFER_button(){
		TRANSFER_button.click();
}

public static void verify_Text(String data){
	Assert.assertTrue(driver.getPageSource().contains(data));
}
}