package testsuite;
import org.testng.annotations.Test;
import PageObjects.*;
import utilities.PageObjectBase;
import org.openqa.selenium.support.PageFactory;
import utilities.Configurations;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import java.util.HashMap;
import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;
import utilities.TestReport;
import java.io.IOException;
import org.testng.Reporter;
import utilities.DataUtil;


/** Conformiq generated test case
	Account_number_and_Verify_Account_number_should_match
*/
public class Account_number_and_Verify_Account_number_should_match extends PageObjectBase
{

	public Account_number_and_Verify_Account_number_should_match()
	{
	}

	private TestReport testReport= new TestReport();


	private StringBuilder overallTestData= new StringBuilder();


	@Test(dataProvider="TestData")
	public void test(final String Step_1_url_FIELD, final String Step_3_Password_textbox_TEXTBOX, final String Step_3_Username_textbox_TEXTBOX, final String Step_6_message_FIELD, final String Step_4_Loan_Provider_dropdown_DROPDOWN_Status, final String Step_4_Threshold_textbox_TEXTBOX_Verification, final String Step_4_Initial_Balance_textbox_TEXTBOX_Verification, final String Step_4_Min_Balance_textbox_TEXTBOX_Verification, final String Step_5_Threshold_textbox_TEXTBOX, final String Step_5_Initial_Balance_textbox_TEXTBOX, final String Step_5_Min_Balance_textbox_TEXTBOX, final String Step_7_Error_FIELD, final String Step_5_First_Name_textbox_TEXTBOX, final String Step_5_Last_Name_textbox_TEXTBOX, final String Step_5_Address_textbox_TEXTBOX, final String Step_5_City_textbox_TEXTBOX, final String Step_5_State_textbox_TEXTBOX, final String Step_5_Zip_Code_textbox_TEXTBOX, final String Step_5_Phone_textbox_TEXTBOX, final String Step_5_SSN_textbox_TEXTBOX, final String Step_5_Username_textbox_TEXTBOX, final String Step_5_Password_textbox_TEXTBOX, final String Step_5_Confirm_textbox_TEXTBOX, final String Step_7_message_FIELD, final String Step_7_First_Name_FIELD, final String Step_7_Last_Name_FIELD, final String Step_7_Adress_FIELD, final String Step_7_City_FIELD, final String Step_7_State_FIELD, final String Step_7_Zip_Code_FIELD, final String Step_7_Phone_FIELD, final String Step_7_SSN_FIELD, final String Step_7_UserName_FIELD, final String Step_7_Password_FIELD, final String Step_7_Confirm_FIELD, final String Step_7_Invalid_Register_FIELD, final String Step_7_Account_FIELD, final String Step_7_Balance_FIELD, final String Step_7_Available_Amount_FIELD, final String Step_9_Account_FIELD, final String Step_9_Balance_FIELD, final String Step_9_Available_Amount_FIELD, final String Step_10_Payee_Name_textbox_TEXTBOX, final String Step_10_Address_textbox_TEXTBOX, final String Step_10_City_textbox_TEXTBOX, final String Step_10_State_textbox_TEXTBOX, final String Step_10_Zip_Code_textbox_TEXTBOX, final String Step_10_Phone_textbox_TEXTBOX, final String Step_10_Account_textbox_TEXTBOX, final String Step_10_Verify_Account_textbox_TEXTBOX, final String Step_10_Amount_textbox_TEXTBOX, final String Step_10_From_Account_dropdown_DROPDOWN, final String Step_11_Bill_Payment_FIELD) throws Exception

	{

	Parabank_Welcome_Page parabank_welcome_page_init=PageFactory.initElements(driver, Parabank_Welcome_Page.class);

	enter_URL_Page enter_url_page_init=PageFactory.initElements(driver, enter_URL_Page.class);

	Register_Page register_page_init=PageFactory.initElements(driver, Register_Page.class);

	Forgot_Login_info_Page forgot_login_info_page_init=PageFactory.initElements(driver, Forgot_Login_info_Page.class);

	Account_Created_Page account_created_page_init=PageFactory.initElements(driver, Account_Created_Page.class);

	Invalid_Credentials_Page invalid_credentials_page_init=PageFactory.initElements(driver, Invalid_Credentials_Page.class);

	Administration_Page administration_page_init=PageFactory.initElements(driver, Administration_Page.class);

	Accounts_Overview_Page accounts_overview_page_init=PageFactory.initElements(driver, Accounts_Overview_Page.class);

	Invalid_Register_Page invalid_register_page_init=PageFactory.initElements(driver, Invalid_Register_Page.class);

	Reject_Register_Page reject_register_page_init=PageFactory.initElements(driver, Reject_Register_Page.class);

	Open_New_Account_Page open_new_account_page_init=PageFactory.initElements(driver, Open_New_Account_Page.class);

	Transfer_Funds_Page transfer_funds_page_init=PageFactory.initElements(driver, Transfer_Funds_Page.class);

	Bill_Payment_Service_Page bill_payment_service_page_init=PageFactory.initElements(driver, Bill_Payment_Service_Page.class);

	Update_Profile_Page update_profile_page_init=PageFactory.initElements(driver, Update_Profile_Page.class);

	Find_Transactions_Page find_transactions_page_init=PageFactory.initElements(driver, Find_Transactions_Page.class);

	Apply_for_a_Loan_Page apply_for_a_loan_page_init=PageFactory.initElements(driver, Apply_for_a_Loan_Page.class);

	Account_Services_Page account_services_page_init=PageFactory.initElements(driver, Account_Services_Page.class);

	Bill_Payment_Complete_Page bill_payment_complete_page_init=PageFactory.initElements(driver, Bill_Payment_Complete_Page.class);

	New_Account_Page new_account_page_init=PageFactory.initElements(driver, New_Account_Page.class);

	Account_Overviews_Page account_overviews_page_init=PageFactory.initElements(driver, Account_Overviews_Page.class);

	Profile_Updated_Page profile_updated_page_init=PageFactory.initElements(driver, Profile_Updated_Page.class);

	Loan_Request_Processed_Page loan_request_processed_page_init=PageFactory.initElements(driver, Loan_Request_Processed_Page.class);

	Account_Overviews_for_Loan_Page account_overviews_for_loan_page_init=PageFactory.initElements(driver, Account_Overviews_for_Loan_Page.class);

	Transaction_Result_Page transaction_result_page_init=PageFactory.initElements(driver, Transaction_Result_Page.class);

	Click_Transaction_Page click_transaction_page_init=PageFactory.initElements(driver, Click_Transaction_Page.class);

	Transaction_Details_Page transaction_details_page_init=PageFactory.initElements(driver, Transaction_Details_Page.class);

	Transaction_Error_Page transaction_error_page_init=PageFactory.initElements(driver, Transaction_Error_Page.class);

	Cal_Page cal_page_init=PageFactory.initElements(driver, Cal_Page.class);
	testReport.createTesthtmlHeader(overallTestData);

	testReport.createHead(overallTestData);

	testReport.putLogo(overallTestData);

	float ne = (float) 0.0;

	testReport.generateGeneralInfo(overallTestData, "Account_number_and_Verify_Account_number_should_match", "TC_Account_number_and_Verify_Account_number_should_match", "",ne);

	testReport.createStepHeader();

	//External Circumstances


	Reporter.log("Step - 1- Perform enter URL Action");

	testReport.fillTableStep("Step 1", "Perform enter URL Action");

	enter_url_page_init.enter_url_field(Step_1_url_FIELD);


	Reporter.log("Step - 2- verify Parabank Welcome screen");

	testReport.fillTableStep("Step 2", "verify Parabank Welcome screen");

	getScreenshot(driver,Configurations.screenshotLocation , "Account_number_and_Verify_Account_number_should_match","Step_2");

	Reporter.log("Step - 3- Fill Customer Login form Parabank Welcome screen");

	testReport.fillTableStep("Step 3", "Fill Customer Login form Parabank Welcome screen");

	parabank_welcome_page_init.set_Password_textbox(Step_3_Password_textbox_TEXTBOX);
	parabank_welcome_page_init.set_Username_textbox(Step_3_Username_textbox_TEXTBOX);
	getScreenshot(driver,Configurations.screenshotLocation , "Account_number_and_Verify_Account_number_should_match","Step_3");

	Reporter.log("Step - 4- click LOG IN button Parabank Welcome screen Customer Login form");

	testReport.fillTableStep("Step 4", "click LOG IN button Parabank Welcome screen Customer Login form");

	parabank_welcome_page_init.click_LOG_IN_button();
	getScreenshot(driver,Configurations.screenshotLocation , "Account_number_and_Verify_Account_number_should_match","Step_4");

	Reporter.log("Step - 5- Fill Cal form Cal screen");

	testReport.fillTableStep("Step 5", "Fill Cal form Cal screen");

	getScreenshot(driver,Configurations.screenshotLocation , "Account_number_and_Verify_Account_number_should_match","Step_5");

	Reporter.log("Step - 6- verify Account Services screen");

	testReport.fillTableStep("Step 6", "verify Account Services screen");

	getScreenshot(driver,Configurations.screenshotLocation , "Account_number_and_Verify_Account_number_should_match","Step_6");

	Reporter.log("Step - 7- Verify Accounts Overview Action");

	testReport.fillTableStep("Step 7", "Verify Accounts Overview Action");

	accounts_overview_page_init.verify_Text(Step_7_Account_FIELD);

	accounts_overview_page_init.verify_Text(Step_7_Balance_FIELD);

	accounts_overview_page_init.verify_Text(Step_7_Available_Amount_FIELD);


	Reporter.log("Step - 8- click Bill Pay hyperlink Account Services screen");

	testReport.fillTableStep("Step 8", "click Bill Pay hyperlink Account Services screen");

	account_services_page_init.click_Bill_Pay_hyperlink();
	getScreenshot(driver,Configurations.screenshotLocation , "Account_number_and_Verify_Account_number_should_match","Step_8");

	Reporter.log("Step - 9- verify Bill Payment Service screen");

	testReport.fillTableStep("Step 9", "verify Bill Payment Service screen");

	getScreenshot(driver,Configurations.screenshotLocation , "Account_number_and_Verify_Account_number_should_match","Step_9");

	Reporter.log("Step - 10- Fill Payee Information form Bill Payment Service screen");

	testReport.fillTableStep("Step 10", "Fill Payee Information form Bill Payment Service screen");

	bill_payment_service_page_init.set_Payee_Name_textbox(Step_10_Payee_Name_textbox_TEXTBOX);
	bill_payment_service_page_init.set_Address_textbox(Step_10_Address_textbox_TEXTBOX);
	bill_payment_service_page_init.set_City_textbox(Step_10_City_textbox_TEXTBOX);
	bill_payment_service_page_init.set_State_textbox(Step_10_State_textbox_TEXTBOX);
	bill_payment_service_page_init.set_Zip_Code_textbox(Step_10_Zip_Code_textbox_TEXTBOX);
	bill_payment_service_page_init.set_Phone_textbox(Step_10_Phone_textbox_TEXTBOX);
	bill_payment_service_page_init.set_Account_textbox(Step_10_Account_textbox_TEXTBOX);
	bill_payment_service_page_init.set_Verify_Account_textbox(Step_10_Verify_Account_textbox_TEXTBOX);
	bill_payment_service_page_init.set_Amount_textbox(Step_10_Amount_textbox_TEXTBOX);
	bill_payment_service_page_init.select_From_Account_dropdown(Step_10_From_Account_dropdown_DROPDOWN);
	getScreenshot(driver,Configurations.screenshotLocation , "Account_number_and_Verify_Account_number_should_match","Step_10");

	Reporter.log("Step - 11- Verify Bill Payment Complete Action");

	testReport.fillTableStep("Step 11", "Verify Bill Payment Complete Action");

	bill_payment_complete_page_init.verify_Text(Step_11_Bill_Payment_FIELD);

	}
	@DataProvider(name = "TestData")
	public Object[][] getData() {
	return DataUtil.getDataFromSpreadSheet("TestData.xlsx", "TCID_15");
}
	@AfterTest
	public void export(){
		testReport.appendtestData(overallTestData);
		testReport.closeStepTable();
		testReport.closeTestHTML(overallTestData);
		driver.close();
		try {
			testReport.writeTestReporthtml(overallTestData, "Account_number_and_Verify_Account_number_should_match");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
